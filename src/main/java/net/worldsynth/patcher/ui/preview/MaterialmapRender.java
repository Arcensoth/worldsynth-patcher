/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
package net.worldsynth.patcher.ui.preview;

import javafx.scene.canvas.GraphicsContext;
import javafx.scene.paint.Color;
import net.worldsynth.datatype.AbstractDatatype;
import net.worldsynth.datatype.DatatypeMaterialmap;
import net.worldsynth.material.MaterialRegistry;
import net.worldsynth.standalone.ui.preview.AbstractPreviewRenderCanvas;

public class MaterialmapRender extends AbstractPreviewRenderCanvas {
	
	private int[][] materialmap;
	
	@Override
	public void pushDataToRender(AbstractDatatype data) {
		DatatypeMaterialmap castData = (DatatypeMaterialmap) data;
		this.materialmap = castData.materialMap;
		paint();
	}
	
	@Override
	public void paint() {
		GraphicsContext g = getGraphicsContext2D();
		g.setFill(Color.gray(0.2));
		g.fillRect(0, 0, getWidth(), getHeight());
		
		if(materialmap != null) {
			
			double xOffset = (getWidth() - materialmap.length)/2;
			double yOffset = (getHeight() - materialmap[0].length)/2;
			
			for(int x = 0; x < materialmap.length; x++) {
				for(int y = 0; y < materialmap[x].length; y++) {
					g.setFill(materialToColor(materialmap[x][y]));
					g.fillRect(x + xOffset, y + yOffset, 1, 1);
				}
			}
		}
	}
	
	private Color materialToColor(int material) {
		return MaterialRegistry.REGISTER.get(material).getFxColor();
	}
}
