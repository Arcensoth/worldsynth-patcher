/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
package net.worldsynth.patcher.ui.preview;

import javafx.scene.canvas.GraphicsContext;
import javafx.scene.paint.Color;
import net.worldsynth.datatype.AbstractDatatype;
import net.worldsynth.datatype.DatatypeColormap;
import net.worldsynth.standalone.ui.preview.AbstractPreviewRenderCanvas;

public class ColormapRender extends AbstractPreviewRenderCanvas {
	
	private float[][][] colormap;
	
	@Override
	public void pushDataToRender(AbstractDatatype data) {
		DatatypeColormap castData = (DatatypeColormap) data;
		this.colormap = castData.colorMap;
		paint();
	}
	
	@Override
	public void paint() {
		GraphicsContext g = getGraphicsContext2D();
		g.setFill(Color.gray(0.2));
		g.fillRect(0, 0, getWidth(), getHeight());
		
		if(colormap != null) {
			
			double xOffset = (getWidth() - colormap.length)/2;
			double yOffset = (getHeight() - colormap[0].length)/2;
			
			for(int x = 0; x < colormap.length; x++) {
				for(int y = 0; y < colormap[x].length; y++) {
					g.setFill(Color.color(colormap[x][y][0], colormap[x][y][1], colormap[x][y][2]));
					g.fillRect(x + xOffset, y + yOffset, 1, 1);
				}
			}
		}
	}
}
