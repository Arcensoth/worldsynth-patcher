/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
package net.worldsynth.patcher.ui.preview;

import javafx.scene.canvas.GraphicsContext;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;
import net.worldsynth.biome.BiomeRegistry;
import net.worldsynth.datatype.AbstractDatatype;
import net.worldsynth.datatype.DatatypeBiomemap;
import net.worldsynth.standalone.ui.preview.AbstractPreviewRenderCanvas;

public class BiomemapRender extends AbstractPreviewRenderCanvas {
	
	private int[][] biomemap;
	
	private int mouseOverBiome = -1;
	
	public BiomemapRender() {
		setOnMouseMoved(e -> {
			mouseOverBiome = biomeAtPixel((int) e.getX(), (int) e.getY());
			paint();
		});
	}
	
	@Override
	public void pushDataToRender(AbstractDatatype data) {
		DatatypeBiomemap castData = (DatatypeBiomemap) data;
		this.biomemap = castData.biomeMap;
		paint();
	}
	
	public void paint() {
		GraphicsContext g = canvas.getGraphicsContext2D();
		g.setFill(Color.gray(0.2));
		g.fillRect(0, 0, getWidth(), getHeight());
		
		if(biomemap != null) {
			
			double xOffset = (getWidth() - biomemap.length)/2;
			double yOffset = (getHeight() - biomemap[0].length)/2;
			
			for(int x = 0; x < biomemap.length; x++) {
				for(int y = 0; y < biomemap[x].length; y++) {
					g.setFill(biomeToColor(biomemap[x][y]));
					g.fillRect(x + xOffset, y + yOffset, 1, 1);
				}
			}
			
			g.setFill(Color.WHITE);
			g.setFont(new Font("TimesRoman", 20));
			g.fillText(BiomeRegistry.getBiomeByInternalId(mouseOverBiome).getName(), 50, getHeight()-40);
		}
	}
	
	private int biomeAtPixel(int x, int y) {
		int xOffset = ((int) getWidth()-biomemap.length)/2;
		int yOffset = ((int) getHeight() - biomemap[0].length)/2;
		
		int mapX = x - xOffset;
		int mapY = y - yOffset;
		
		if(mapX > 0 && mapY > 0 && mapX < biomemap.length && mapY < biomemap[0].length) {
			return biomemap[mapX][mapY];
		}
		
		return -1;
	}
	
	private Color biomeToColor(int biomeId) {
		return BiomeRegistry.getBiomeByInternalId(biomeId).getFxColor();
	}
}
