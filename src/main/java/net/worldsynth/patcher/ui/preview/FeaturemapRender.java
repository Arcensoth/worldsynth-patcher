/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
package net.worldsynth.patcher.ui.preview;


import javafx.scene.canvas.GraphicsContext;
import javafx.scene.paint.Color;
import net.worldsynth.datatype.AbstractDatatype;
import net.worldsynth.datatype.DatatypeFeaturemap;
import net.worldsynth.standalone.ui.preview.AbstractPreviewRenderCanvas;

public class FeaturemapRender extends AbstractPreviewRenderCanvas {

	private double[][] points;
	
	public double x, z, width, length;
	
	@Override
	public void pushDataToRender(AbstractDatatype data) {
		DatatypeFeaturemap castData = (DatatypeFeaturemap) data;
		this.points = castData.points;
		this.x = castData.x;
		this.z = castData.z;
		this.width = castData.width;
		this.length = castData.length;
		paint();
	}
	
	@Override
	public void paint() {
		GraphicsContext g = getGraphicsContext2D();
		g.setFill(Color.gray(0.2));
		g.fillRect(0, 0, getWidth(), getHeight());
		
		if(points != null) {
			double resPitch = 250.0 / Math.max(width, length);
			
			double xOffset = getWidth()/2 - (width*resPitch)/2.0;
			double yOffset = getHeight()/2 - (length*resPitch)/2.0;
			
			g.setFill(Color.BLACK);
			g.fillRect(xOffset, yOffset, width*resPitch, length*resPitch);
			
			for(double[] p: points) {
				g.setFill(Color.WHITE);
				g.fillRect((p[0]-x) * resPitch + xOffset - 1, (p[1]-z) * resPitch + yOffset - 1, 3, 3);
			}
		}
	}
}
