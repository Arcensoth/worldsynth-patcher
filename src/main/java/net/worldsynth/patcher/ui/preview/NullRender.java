/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
package net.worldsynth.patcher.ui.preview;

import javafx.scene.canvas.GraphicsContext;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;
import javafx.scene.text.TextAlignment;
import net.worldsynth.datatype.AbstractDatatype;
import net.worldsynth.standalone.ui.preview.AbstractPreviewRenderCanvas;

public class NullRender extends AbstractPreviewRenderCanvas {
	
	@Override
	public void paint() {
		GraphicsContext g = getGraphicsContext2D();
		g.setFill(Color.gray(0.2));
		g.fillRect(0, 0, getWidth(), getHeight());
		
		g.setFill(Color.WHITE);
		g.setFont(new Font("TimesRoman", 40));
		g.setTextAlign(TextAlignment.CENTER);
		g.fillText("NULL", getWidth()/2, getHeight()/2-40);
		g.fillText("NO DATA TO RENDER", getWidth()/2, getHeight()/2+40);
	}
	
	@Override
	public void pushDataToRender(AbstractDatatype data) {
	}
}
