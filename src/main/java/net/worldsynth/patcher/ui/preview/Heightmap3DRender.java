/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
package net.worldsynth.patcher.ui.preview;

import net.worldsynth.datatype.AbstractDatatype;
import net.worldsynth.datatype.DatatypeHeightmap;
import net.worldsynth.glpreview.fx.Heightmap3dViewer;
import net.worldsynth.standalone.ui.preview.AbstractPreviewRender;

public class Heightmap3DRender extends AbstractPreviewRender {
	
	private final Heightmap3dViewer heightmapViewer;
	
	public Heightmap3DRender() {
		super();
		heightmapViewer = new Heightmap3dViewer();
		heightmapViewer.setPrefSize(450, 450);
		getChildren().add(heightmapViewer);
	}
	
	@Override
    protected void layoutChildren() {
        super.layoutChildren();
        final double x = snappedLeftInset();
        final double y = snappedTopInset();
        heightmapViewer.setLayoutX(x);
        heightmapViewer.setLayoutY(y);
    }
	
	@Override
	public void pushDataToRender(AbstractDatatype data) {
		DatatypeHeightmap castData = (DatatypeHeightmap) data;
		heightmapViewer.setHeightmap(castData.getHeightmap(), 255, Math.max(castData.width, castData.length));
	}
}
