/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
package net.worldsynth.patcher.ui.syntheditor.history;

public class EditorHistoryEventModuleAddRemove extends AbstractEditorHistoryEvent {
	
	private Object[] addedSubjects;
	private Object[] removedSubjects;
	
	public EditorHistoryEventModuleAddRemove(Object[] addedSubjects, Object[] removedSubjects) {
		this.addedSubjects = addedSubjects;
		this.removedSubjects = removedSubjects;
	}
	
	public Object[] getAddedSubjects() {
		return addedSubjects;
	}
	
	public Object[] getRemovedSubjects() {
		return removedSubjects;
	}
}
