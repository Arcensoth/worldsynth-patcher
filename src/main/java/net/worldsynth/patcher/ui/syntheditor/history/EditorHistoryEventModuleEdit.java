/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
package net.worldsynth.patcher.ui.syntheditor.history;

import net.worldsynth.modulewrapper.ModuleWrapper;
import net.worldsynth.synth.io.Element;

public class EditorHistoryEventModuleEdit extends AbstractEditorHistoryEvent {
	
	private ModuleWrapper subject;
	private Element oldParameterElemetn;
	private Element newParameterElement;
	
	public EditorHistoryEventModuleEdit(ModuleWrapper subjects, Element oldParameterElement, Element newParameterElement) {
		this.subject = subjects;
		this.oldParameterElemetn = oldParameterElement;
		this.newParameterElement = newParameterElement;
	}
	
	public ModuleWrapper getSubject() {
		return subject;
	}
	
	public Element getOldParameterElement() {
		return oldParameterElemetn;
	}
	
	public Element getNewParameterElement() {
		return newParameterElement;
	}
}
