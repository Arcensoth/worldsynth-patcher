/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
package net.worldsynth.patcher.ui.fx.syntheditor;

import java.awt.Polygon;
import java.util.ArrayList;

import net.worldsynth.modulewrapper.ModuleWrapper;

public class SelectionLasso {
	
	private ArrayList<double[]> points = new ArrayList<double[]>();
	
	public SelectionLasso(double startCoordinateX, double startCoordinateY) {
		double[] newPoint = {startCoordinateX, startCoordinateY};
		points.add(newPoint);
		points.add(newPoint.clone());
	}
	
	public void setCurrentCoordinate(double coordinateX, double coordinateY) {
		double[] latestPoint = points.get(points.size() - 2);
		double[] currentPoint = {coordinateX, coordinateY};
		points.set(points.size() - 1, currentPoint);
		if(dist(latestPoint, currentPoint) > 10) {
			points.add(currentPoint.clone());
		}
	}
	
	public double[][] getPoints() {
		double[][] pointsArray = new double[0][2];
		pointsArray = this.points.toArray(pointsArray);
		return pointsArray;
	}
	
	public ModuleWrapper[] getDevicesInside(SynthEditorPane synthEditor) {
		ArrayList<ModuleWrapper> containedDeviceList = new ArrayList<ModuleWrapper>();
		
		//Build polygon
		Polygon polygon = new Polygon();
		for(double[] p: points) {
			polygon.addPoint((int)p[0], (int)p[1]);
		}
		
		//check for containment
		for(ModuleWrapper d: synthEditor.getSynth().getWrapperList()) {
			if(polygon.contains(d.posX, d.posY, d.wrapperWidth, d.wrapperHeight)) {
				containedDeviceList.add(d);
			}
		}
		
		//Return contained devices
		ModuleWrapper[] list = new ModuleWrapper[0];
		return containedDeviceList.toArray(list);
	}
	
	private float dist(double[] p1, double[] p2) {
		double dx = p2[0] - p1[0];
		double dy = p2[1] - p1[1];
		return (float) Math.sqrt((dx * dx) + (dy * dy));
	}
}
