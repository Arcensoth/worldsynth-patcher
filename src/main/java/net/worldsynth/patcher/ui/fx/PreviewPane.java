/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
package net.worldsynth.patcher.ui.fx;


import java.util.concurrent.ExecutionException;

import javafx.beans.binding.Bindings;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.geometry.Pos;
import javafx.scene.control.ComboBox;
import javafx.scene.control.ToggleButton;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.FlowPane;
import net.worldsynth.datatype.AbstractDatatype;
import net.worldsynth.datatype.DatatypeBiomemap;
import net.worldsynth.datatype.DatatypeBlockspace;
import net.worldsynth.datatype.DatatypeColormap;
import net.worldsynth.datatype.DatatypeFeaturemap;
import net.worldsynth.datatype.DatatypeFeaturespace;
import net.worldsynth.datatype.DatatypeHeightmap;
import net.worldsynth.datatype.DatatypeHeightmapOverlay;
import net.worldsynth.datatype.DatatypeMaterialmap;
import net.worldsynth.datatype.DatatypeObjects;
import net.worldsynth.datatype.DatatypeScalar;
import net.worldsynth.datatype.DatatypeValuespace;
import net.worldsynth.datatype.DatatypeVectormap;
import net.worldsynth.extent.WorldExtent;
import net.worldsynth.extent.WorldExtentManager;
import net.worldsynth.module.ModuleOutput;
import net.worldsynth.module.ModuleOutputRequest;
import net.worldsynth.modulewrapper.ModuleWrapper;
import net.worldsynth.modulewrapper.ModuleWrapperIO;
import net.worldsynth.patcher.build.BuildTask;
import net.worldsynth.patcher.ui.preview.BiomemapRender;
import net.worldsynth.patcher.ui.preview.Blockspace3DRender;
import net.worldsynth.patcher.ui.preview.ColormapRender;
import net.worldsynth.patcher.ui.preview.FeaturemapRender;
import net.worldsynth.patcher.ui.preview.Featurespace3DRender;
import net.worldsynth.patcher.ui.preview.Heightmap3DRender;
import net.worldsynth.patcher.ui.preview.HeightmapColorscaleRender;
import net.worldsynth.patcher.ui.preview.HeightmapGrayscaleRender;
import net.worldsynth.patcher.ui.preview.HeightmapOverlay3DRender;
import net.worldsynth.patcher.ui.preview.MaterialmapRender;
import net.worldsynth.patcher.ui.preview.NullRender;
import net.worldsynth.patcher.ui.preview.Objects3DRender;
import net.worldsynth.patcher.ui.preview.ScalarRender;
import net.worldsynth.patcher.ui.preview.UndefinedRender;
import net.worldsynth.patcher.ui.preview.VectormapRender;
import net.worldsynth.standalone.ui.preview.AbstractPreviewRender;
import net.worldsynth.synth.Synth;
import net.worldsynth.util.event.build.BuildStatusListener;

public class PreviewPane extends BorderPane {
	
	private WorldExtentManager currentExtentsManager = null;
	private ComboBox<WorldExtent> extentSelector = new ComboBox<WorldExtent>();
	
	private BuildTask mainBuildTask = null;
	
	private Synth currentPreviewSynth = null;
	private ModuleWrapper currentPreviewWrapper = null;
	
	private HeightmapPreviewType heightmapPreviewStyle = HeightmapPreviewType.GRAYSCALE;
	
	private AbstractPreviewRender previewRender;
	
	public PreviewPane() {
		setPreviewRender(new NullRender());
		
		extentSelector.setMaxWidth(Double.MAX_VALUE);
		setTop(extentSelector);
		
		ToggleButton grayscaleButton = new ToggleButton("Grayscale");
		grayscaleButton.setMinWidth(100.0);
		ToggleButton colorscaleButton = new ToggleButton("Colorscale");
		colorscaleButton.setMinWidth(100.0);
		ToggleButton openglButton = new ToggleButton("3D");
		openglButton.setMinWidth(100.0);
		
		grayscaleButton.setSelected(true);
		grayscaleButton.setOnAction(e -> {
			heightmapPreviewStyle = HeightmapPreviewType.GRAYSCALE;
			colorscaleButton.setSelected(false);
			openglButton.setSelected(false);
			updatePreview();
		});
		colorscaleButton.setOnAction(e -> {
			heightmapPreviewStyle = HeightmapPreviewType.COLORSCALE;
			grayscaleButton.setSelected(false);
			openglButton.setSelected(false);
			updatePreview();
		});
		openglButton.setOnAction(e -> {
			heightmapPreviewStyle = HeightmapPreviewType.OPENGL;
			grayscaleButton.setSelected(false);
			colorscaleButton.setSelected(false);
			updatePreview();
		});
		
		FlowPane previewStylesCollection = new FlowPane(grayscaleButton, colorscaleButton, openglButton);
		previewStylesCollection.setAlignment(Pos.CENTER);
		setBottom(previewStylesCollection);
	}
	
	public void updatePreview() {
		updatePreview(currentPreviewSynth, currentPreviewWrapper);
	}
	
	public void updatePreview(Synth synth, ModuleWrapper wrapper) {
		currentPreviewSynth = synth;
		currentPreviewWrapper = wrapper;
		
		if(wrapper != null && currentExtentsManager.getCurrentWorldExtent() != null) {
			if(wrapper.wrapperOutputs.size() > 0) {
				ModuleWrapperIO primaryWrapperOutput = wrapper.wrapperOutputs.get(wrapper.module.getOutputs()[0].getName());
				if(primaryWrapperOutput != null) {
					double x = currentExtentsManager.getCurrentWorldExtent().getX();
					double y = currentExtentsManager.getCurrentWorldExtent().getY();
					double z = currentExtentsManager.getCurrentWorldExtent().getZ();
					
					double width = currentExtentsManager.getCurrentWorldExtent().getWidth();
					double length = currentExtentsManager.getCurrentWorldExtent().getLength();
					double height = currentExtentsManager.getCurrentWorldExtent().getHeight();
					
					double resolutionPitch = Math.max(Math.max(width, length) / WorldExtentManager.previewRes, 1.0);
					
					AbstractDatatype requestData = primaryWrapperOutput.getDatatype().getPreviewDatatype(x, y, z, width, height, length, resolutionPitch);
					ModuleOutput output = (ModuleOutput) primaryWrapperOutput.getIO();
					ModuleOutputRequest request = new ModuleOutputRequest(output, requestData);
					
					buildOnNewTreadAndPushToPreview(synth, wrapper, request, null);
				}
			}
		}
	}
	
	public void setExtentManager(WorldExtentManager manager) {
		if(currentExtentsManager != null) {
			currentExtentsManager.currentWorldExtentProperty().removeListener(extentManagerSelectionChangeListener);
			Bindings.unbindBidirectional(extentSelector.valueProperty(), currentExtentsManager.currentWorldExtentProperty());
			extentSelector.valueProperty().set(null);
			extentSelector.setItems(FXCollections.emptyObservableList());
		}
		currentExtentsManager = manager;
		if(manager != null) {
			currentExtentsManager.currentWorldExtentProperty().addListener(extentManagerSelectionChangeListener);
			extentSelector.setItems(manager.getObservableExtentsList());
			Bindings.bindBidirectional(extentSelector.valueProperty(), currentExtentsManager.currentWorldExtentProperty());
		}
	}
	
	/**
	 * This listener listens for change in the current worldextent in the extents manager.<br>
	 * The listener is registered to the appropriate properties inside {@link #setExtentManager(WorldExtentManager) setExtentManager}.
	 */
	private ChangeListener<WorldExtent> extentManagerSelectionChangeListener = (ObservableValue<? extends WorldExtent> observable, WorldExtent oldValue, WorldExtent newValue) -> {
		if(newValue == null) {
			return;
		}
		//extentSelector.getItems();
		updatePreview();
	};
	
	private void setPreviewRender(AbstractPreviewRender render) {
		previewRender = render;
		setCenter(previewRender);
	}
	
	private AbstractPreviewRender getRenderForDatatype(AbstractDatatype datatype) {
		if(datatype instanceof DatatypeHeightmap && heightmapPreviewStyle == HeightmapPreviewType.GRAYSCALE) {
			return new HeightmapGrayscaleRender();
		}
		else if(datatype instanceof DatatypeHeightmap && heightmapPreviewStyle == HeightmapPreviewType.COLORSCALE) {
			return new HeightmapColorscaleRender();
		}
		else if(datatype instanceof DatatypeHeightmap && heightmapPreviewStyle == HeightmapPreviewType.OPENGL) {
			return new Heightmap3DRender();
		}
		else if(datatype instanceof DatatypeScalar) {
			return new ScalarRender();
		}
		else if(datatype instanceof DatatypeColormap) {
			return new ColormapRender();
		}
		else if(datatype instanceof DatatypeHeightmapOverlay) {
			return new HeightmapOverlay3DRender();
		}
		else if(datatype instanceof DatatypeMaterialmap) {
			return new MaterialmapRender();
		}
		else if(datatype instanceof DatatypeBiomemap) {
			return new BiomemapRender();
		}
		else if(datatype instanceof DatatypeBlockspace) {
			return new Blockspace3DRender();
		}
		else if(datatype instanceof DatatypeObjects) {
			return new Objects3DRender();
		}
		else if(datatype instanceof DatatypeVectormap) {
			return new VectormapRender();
		}
		else if(datatype instanceof DatatypeFeaturemap) {
			return new FeaturemapRender();
		}
		else if(datatype instanceof DatatypeFeaturespace) {
			return new Featurespace3DRender();
		}
		//TODO implement a render for valuespace
		else if(datatype instanceof DatatypeValuespace) {
			return new UndefinedRender();
		}
		else if(datatype == null) {
			return new NullRender();
		}
		else {
			AbstractPreviewRender render = datatype.getPreviewRender();
			if(render == null) {
				return new UndefinedRender();
			}
			return render;
		}
	}
	
	private void buildOnNewTreadAndPushToPreview(Synth synth, ModuleWrapper wrapper, ModuleOutputRequest request, BuildStatusListener buildListener) {
		if(mainBuildTask != null) {
			mainBuildTask.cancel();
		}
		mainBuildTask = new BuildTask(synth, wrapper, request, e -> {
			WorldSynthEditorController.instance.infobar.updateInfoText(e.getStatus().toString());
		});
		
		mainBuildTask.setOnSucceeded(e -> {
			System.out.println("Build succeded");
			
			try {
				AbstractDatatype builtData = mainBuildTask.get();
				AbstractPreviewRender newRender = getRenderForDatatype(builtData);
				if(!previewRender.getClass().equals(newRender.getClass())) {
					setPreviewRender(newRender);
				}
				
				previewRender.pushDataToRender(builtData);
			} catch (InterruptedException | ExecutionException e1) {
				e1.printStackTrace();
			}
			
			mainBuildTask = null;
		});
		
		new Thread(mainBuildTask).start();
	}

	public enum HeightmapPreviewType {
		GRAYSCALE,
		COLORSCALE,
		OPENGL;
	}
}
