/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
package net.worldsynth.patcher.ui.fx;

import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.ObservableList;
import javafx.event.Event;
import javafx.event.EventDispatchChain;
import javafx.event.EventDispatcher;
import javafx.event.EventHandler;
import javafx.scene.control.ContextMenu;
import javafx.scene.control.MenuItem;
import javafx.scene.control.SelectionMode;
import javafx.scene.control.TextField;
import javafx.scene.control.TreeCell;
import javafx.scene.control.TreeItem;
import javafx.scene.control.TreeView;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.scene.input.MouseButton;
import javafx.scene.input.MouseEvent;
import javafx.util.Callback;
import net.worldsynth.modulewrapper.ModuleWrapper;
import net.worldsynth.patcher.ui.fx.syntheditor.SynthEditorPane;
import net.worldsynth.standalone.ui.stage.ModuleParametersStage;

public class SynthTreeView extends TreeView<ModuleWrapper> {
	
	private boolean ignoreSelectionUpdate = false;
	
	public SynthTreeView() {
		super();
		
		setEventDispatcher(new EventDispatcher() {
			
			@Override
			public Event dispatchEvent(Event event, EventDispatchChain tail) {
				if (event instanceof MouseEvent) {
					MouseEvent mouseEvent = (MouseEvent) event;
					if (mouseEvent.getButton() == MouseButton.SECONDARY) {
						event.consume();
					} else {
						event = tail.dispatchEvent(event);
					}
				} else {
					event = tail.dispatchEvent(event);
				}
				return event;
			}
		});
		
		setEditable(true);
		setCellFactory(new Callback<TreeView<ModuleWrapper>, TreeCell<ModuleWrapper>>() {
			@Override
			public TreeCell<ModuleWrapper> call(TreeView<ModuleWrapper> param) {
				return new TextFieldTreeCellImpl();
			}
		});
		
		setShowRoot(false);
		getSelectionModel().setSelectionMode(SelectionMode.MULTIPLE);
		getSelectionModel().selectedItemProperty().addListener((ObservableValue<? extends TreeItem<ModuleWrapper>> observable, TreeItem<ModuleWrapper> oldValue, TreeItem<ModuleWrapper> newValue) -> {
			if(ignoreSelectionUpdate) {
				return;
			}
			
			ObservableList<TreeItem<ModuleWrapper>> selection = getSelectionModel().getSelectedItems();
			int size = selection.size();
			ModuleWrapper[] selectedDevices = new ModuleWrapper[size];
			for(int i = 0; i < selectedDevices.length; i++) {
				TreeItem<ModuleWrapper> item = selection.get(i);
				selectedDevices[i] = item.getValue();
			}
			WorldSynthEditorController.instance.currentSynthEditor.setSelectedWrappers(selectedDevices, true);
		});
	}
	
	public void clearTreeView() {
		ignoreSelectionUpdate = true;
		setRoot(new TreeItem<ModuleWrapper>());
		ignoreSelectionUpdate = false;
	}
	
	public void setSynthEditor(SynthEditorPane synthEditor) {
		ignoreSelectionUpdate = true;
		setRoot(synthEditor.getDeviceTree());
		setSelection(synthEditor.getSelectedWrappers());
		ignoreSelectionUpdate = false;
	}
	
	public void setSelection(ModuleWrapper[] selectedDevices) {
		//Update tree selection
		ignoreSelectionUpdate = true;
		System.out.println("Clear tree selection");
		getSelectionModel().clearSelection();
		System.out.println("Set tree selection");
		for(ModuleWrapper d: selectedDevices) {
			for(TreeItem<ModuleWrapper> item: getRoot().getChildren()) {
				if(item.getValue() == d) {
					getSelectionModel().select(item);
				}
			}
		}
		ignoreSelectionUpdate = false;
	}
	
	private final class TextFieldTreeCellImpl extends TreeCell<ModuleWrapper> {
		private TextField textField;
		private ContextMenu contextMenu;
		
		public TextFieldTreeCellImpl() {
			MenuItem parametersItem = new MenuItem("Parameters");
			parametersItem.setOnAction(e -> {
				new ModuleParametersStage(getItem());
			});
//			MenuItem bypassItem = new MenuItem("Bypass");
//			bypassItem.setDisable(!getItem().isBypassable());
//			bypassItem.setOnAction(e -> {
//				getItem().setBypassed(!getItem().isBypassed());
//			});
			contextMenu = new ContextMenu(parametersItem);
			setContextMenu(contextMenu);
		}
		
		@Override
		public void startEdit() {
			super.startEdit();
			
			if (textField == null) {
				createTextField();
			}
			else {
				textField.setText(getItem().getCustomName());
			}
			setText(null);
			setGraphic(textField);
			textField.positionCaret(textField.getText().length());
			textField.requestFocus();
		}
		
		@Override
		public void cancelEdit() {
			super.cancelEdit();
			setText(getItem().toString());
			setGraphic(getTreeItem().getGraphic());
		}
		
		@Override
		public void updateItem(ModuleWrapper item, boolean empty) {
			super.updateItem(item, empty);
			
			if (empty) {
				setText(null);
				setGraphic(null);
			}
			else {
				if (isEditing()) {
					if (textField != null) {
						textField.setText(item.getCustomName());
					}
					setText(null);
					setGraphic(textField);
				}
				else {
					setText(getString());
					setGraphic(getTreeItem().getGraphic());
				}
			}
		}
		
		private void createTextField() {
			textField = new TextField(getItem().getCustomName());
			textField.setOnKeyReleased(new EventHandler<KeyEvent>() {
				@Override
				public void handle(KeyEvent t) {
					if (t.getCode() == KeyCode.ENTER) {
						getItem().setCustomName(textField.getText());
						commitEdit(getItem());
						// cancelEdit();
					}
					else if (t.getCode() == KeyCode.ESCAPE) {
						cancelEdit();
					}
				}
			});
			textField.focusedProperty().addListener(new ChangeListener<Boolean>() {

				@Override
				public void changed(ObservableValue<? extends Boolean> observable, Boolean oldValue, Boolean newValue) {
					if(newValue == false) {
						cancelEdit();
					}
				}
			});
		}
		
		private String getString() {
			return getItem() == null ? "" : getItem().toString();
		}
	}
}
