/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
package net.worldsynth.patcher.ui.fx;

import javafx.scene.control.Label;
import javafx.scene.layout.BorderPane;

public class Infobar extends BorderPane {
	
	private final Label infoLabel;
	
	public Infobar() {
		infoLabel = new Label("Welcome to WorldSynth - Version " + WorldSynthPatcher.VERSION);
		setCenter(infoLabel);
	}
	
	public void updateInfoText(String text) {
		infoLabel.setText(text);
	}
}
