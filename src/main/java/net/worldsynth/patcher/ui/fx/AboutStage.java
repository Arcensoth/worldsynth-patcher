/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
package net.worldsynth.patcher.ui.fx;

import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.geometry.Insets;
import javafx.scene.Scene;
import javafx.scene.control.Tab;
import javafx.scene.control.TabPane;
import javafx.scene.control.TabPane.TabClosingPolicy;
import javafx.scene.control.TextArea;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.GridPane;
import javafx.stage.Stage;
import javafx.stage.StageStyle;

public class AboutStage extends Stage {
	
	public AboutStage() {
		initStyle(StageStyle.UNDECORATED);
		setResizable(false);
		setAlwaysOnTop(true);
		
		
		ImageView patcherImage = new ImageView();
		patcherImage.setFitWidth(700);
		patcherImage.setPreserveRatio(true);
		patcherImage.setImage(new Image(getClass().getResourceAsStream("Splash.jpg")));
		
		TabPane patcherInfoTabPane = new TabPane();
		patcherInfoTabPane.setTabClosingPolicy(TabClosingPolicy.UNAVAILABLE);
		
		//---------------------------------------------------------------//
		
		TextArea patcherAboutText = new TextArea("About\n"
				+ "WorldSynth Patcher\n"
				+ "WorldSynth version: " + WorldSynthPatcher.VERSION + "\n\n"
				+ "WorldSynth webpage:\twww.worldsynth.net\n"
				+ "Worldsynth reddit:\t\twww.reddit.worldsynth.net\n"
				+ "Worldsynth discord:\twww.discord.worldsynth.net");
		patcherAboutText.setPrefWidth(500);
		patcherAboutText.setPrefHeight(250);
		patcherAboutText.setEditable(false);
		patcherAboutText.setStyle(""
				+ "-fx-font-size: 20;"
				+ "-fx-focus-color: transparent;"
				+ "-fx-faint-focus-color: transparent;");
		
		patcherInfoTabPane.getTabs().add(new Tab("Patcher", patcherAboutText));
		
		//---------------------------------------------------------------//
		
		String sysInfo = "";
		for(Object key: System.getProperties().keySet()) {
			sysInfo += key + ":    " + System.getProperty((String) key) + "\n";
		}
		TextArea systemInfoText = new TextArea(sysInfo);
		systemInfoText.setPrefWidth(500);
		systemInfoText.setPrefHeight(250);
		systemInfoText.setEditable(false);
		systemInfoText.setStyle(""
				+ "-fx-font-size: 12;"
				+ "-fx-focus-color: transparent;"
				+ "-fx-faint-focus-color: transparent;");
		
		patcherInfoTabPane.getTabs().add(new Tab("System", systemInfoText));
		
		//---------------------------------------------------------------//
		
		//TODO Credits tab
		
		//---------------------------------------------------------------//
		
		GridPane aboutPane = new GridPane();
		aboutPane.setPadding(new Insets(20.0));
		aboutPane.add(patcherImage, 0, 0, 3, 1);
		aboutPane.add(patcherInfoTabPane, 0, 1, 3, 1);
		
		Scene aboutScene = new Scene(aboutPane);
		aboutPane.getStylesheets().add(WorldSynthPatcher.stylesheet);
		setScene(aboutScene);
		
		focusedProperty().addListener(new ChangeListener<Boolean>() {

			@Override
			public void changed(ObservableValue<? extends Boolean> observable, Boolean oldValue, Boolean newValue) {
				if(newValue == false) {
					close();
				}
			}
		});
		
		show();
	}
}