/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
package net.worldsynth.patcher.ui.fx.syntheditor;

import java.io.File;
import java.util.ArrayList;

import javafx.scene.control.ContextMenu;
import javafx.scene.control.Menu;
import javafx.scene.control.MenuItem;
import javafx.scene.control.SeparatorMenuItem;
import javafx.stage.FileChooser;
import javafx.stage.FileChooser.ExtensionFilter;
import net.worldsynth.common.WorldSynthCore;
import net.worldsynth.module.AbstractModule;
import net.worldsynth.module.AbstractModuleRegister.ModuleEntry;
import net.worldsynth.modulewrapper.ModuleWrapper;
import net.worldsynth.patcher.WorldSynthStatTracker;
import net.worldsynth.patcher.ui.fx.WorldSynthPatcher;
import net.worldsynth.synth.Synth;
import net.worldsynth.synth.io.ProjectReader;

public class ModuleSelectionContextMenu extends ContextMenu {

	private SynthEditorPane synthEditor;
	
	public ModuleSelectionContextMenu(SynthEditorPane synthEditor) {
		this.synthEditor = synthEditor;
		
		synthEditor.setOnMousePressed(e -> {
			hide();
		});
		
		//Synth
		getItems().add(new SynthImportMenuItem());
		getItems().add(new SeparatorMenuItem());
		
		//Build the module menu structure
		ModuleMenuStructure structure = new ModuleMenuStructure("root");
		for(ModuleEntry moduleEntry: WorldSynthCore.moduleRegister.getRegisteredModuleEntries()) {
			structure.addModuleToStructure(moduleEntry);
		}
		
		//Convert the module menu structure to JMenu
		for(ModuleMenuStructure item: structure.subStructures) {
			MenuItem newRootItem = convertStructureToMenuItem(item);
			getItems().add(newRootItem);
		}
	}
	
	private MenuItem convertStructureToMenuItem(ModuleMenuStructure structure) {
		MenuItem item;
		
		//If item is a module
		if(structure.module != null) {
			item = new MenuItem(structure.itemName);
			item.setOnAction(e -> {
				try {
					WorldSynthStatTracker.incrementModuleUse(structure.module);
					synthEditor.setTempDevice(new ModuleWrapper(structure.module, synthEditor.getSynth(), 0.0, 0.0));
				} catch(Exception e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
			});
			
			return item;
		}
		
		//Else item is a parent item
		else {
			item = new Menu(structure.itemName);
			for(ModuleMenuStructure subStructure: structure.subStructures) {
				MenuItem subItem = convertStructureToMenuItem(subStructure);
				((Menu) item).getItems().add(subItem);
			}
			return item;
		}
	}
	
	private class ModuleMenuStructure {
		ArrayList<ModuleMenuStructure> subStructures = new ArrayList<ModuleMenuStructure>();
		
		Class<? extends AbstractModule> module = null;
		String itemName = null;
		
		public ModuleMenuStructure(String itemName) {
			this.itemName = itemName;
		}

		public ModuleMenuStructure(Class<? extends AbstractModule> module, String moduleName) {
			this.module = module;
			this.itemName = moduleName;
		}
		
		private void addModuleToStructure(Class<? extends AbstractModule> moduleClass, String menuPath, String moduleName) {
			if(menuPath.startsWith("\\")) {
				menuPath = menuPath.replaceFirst("\\\\", "");
				String subStructureName = menuPath.split("\\\\", 2)[0];
				menuPath = menuPath.replaceFirst(subStructureName, "");
				
				for(ModuleMenuStructure subStructure: subStructures) {
					if(subStructure.itemName.equals(subStructureName)) {
						subStructure.addModuleToStructure(moduleClass, menuPath, moduleName);
						return;
					}
				}
				
				ModuleMenuStructure newSubItem = new ModuleMenuStructure(subStructureName);
				newSubItem.addModuleToStructure(moduleClass, menuPath, moduleName);
				subStructures.add(newSubItem);
				return;
			}
			else {
				subStructures.add(new ModuleMenuStructure(moduleClass, moduleName));
			}
		}
		
		public void addModuleToStructure(ModuleEntry entry) {
			String menuPath = entry.getModuleMenuPath();
			String moduleName = entry.getModuleName();
			Class<? extends AbstractModule> moduleClass = entry.getModuleClass();
			
			addModuleToStructure(moduleClass, menuPath, moduleName);
		}
	}
	
	private class SynthImportMenuItem extends Menu {

		public SynthImportMenuItem() {
			super("Synth import");
			
			getItems().add(new BlueprintItem());
			getItems().add(new MacroItem());
		}
	}
	
	private class BlueprintItem extends MenuItem {
		
		public BlueprintItem() {
			super("Blueprint");
			
			setOnAction(e -> {
				File synthFile = getSynthFile();
				if(synthFile == null) {
					return;
				}
				else if(!synthFile.exists()) {
					return;
				}
				
				Synth blueprintSynth = ProjectReader.readSynthFromFile(synthFile);
				synthEditor.setTempSynth(new TempSynth(blueprintSynth.toElement(), synthEditor.getSynth()));
			});
		}
	}
	
	private class MacroItem extends MenuItem {

		public MacroItem() {
			super("Macro");
			
			setOnAction(e -> {
				File synthFile = getSynthFile();
				if(synthFile == null) {
					return;
				}
				else if(!synthFile.exists()) {
					return;
				}
				
				ModuleWrapper macro = new ModuleWrapper(synthFile, synthEditor.getSynth(), 0.0f, 0.0f);
				synthEditor.setTempDevice(macro);
			});
		}
	}
	
	private File getSynthFile() {
		FileChooser fileChooser = new FileChooser();
		fileChooser.setTitle("Open WorldSynth project");
		fileChooser.getExtensionFilters().add(new ExtensionFilter("WorldSynth", "*.wsynth"));
		
		File synthFile = fileChooser.showOpenDialog(WorldSynthPatcher.primaryStage);
		
		return synthFile;
	}
}