/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
package net.worldsynth.patcher.ui.fx;

import java.awt.Window.Type;

import javax.swing.ImageIcon;
import javax.swing.JFrame;
import javax.swing.JLabel;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.image.Image;
import javafx.scene.input.KeyEvent;
import javafx.stage.Stage;
import net.worldsynth.common.Commons;
import net.worldsynth.common.WorldSynthCore;
import net.worldsynth.extent.WorldExtentManager;
import net.worldsynth.module.AbstractModuleRegister;
import net.worldsynth.patcher.WorldSynthStatTracker;
import net.worldsynth.patcher.ui.fx.syntheditor.SynthEditorPane;
import net.worldsynth.standalone.ui.stage.StageManager;

public class WorldSynthPatcher extends Application {
	
	public static final String VERSION = "N/A (A1.2.0-dev)";
	
	public static String stylesheet;
	public static WorldSynthPatcher instance;
	public static Stage primaryStage;
	
	private static AbstractModuleRegister[] injectedModuleRegisters;
	
	public static void main(String[] args) {
		injectedModuleRegisters = null;
		launch(args);
	}
	
	/**
	 * Start Patcher with module registers passed as arguments in addition to addons loaded from addon jars.
	 * This is practical for loading modules in addons under development.
	 * 
	 * @param args as in main args
	 * @param moduleRegisters instances of module registers to be loaded
	 */
	public static void startPatcher(String[] args, AbstractModuleRegister ... moduleRegisters) {
		injectedModuleRegisters = moduleRegisters;
		launch(args);
	}
	
	@Override
	public void start(Stage primaryStage) throws Exception {
		instance = this;
		
		System.out.println("JRE: " + System.getProperty("java.version"));
		System.out.println("Starting WorldSynth standalone editor");
		
		JFrame splash = new JFrame();
		if(!Commons.systemIsMac()) {
			splash.setLocationRelativeTo(null);
			splash.setUndecorated(true);
			splash.setType(Type.UTILITY);
			splash.add(new JLabel(new ImageIcon(getClass().getResource("Splash.jpg"))));
			splash.pack();
			splash.setLocation(splash.getLocation().x-splash.getWidth()/2, splash.getLocation().y-splash.getHeight()/2);
			splash.setVisible(true);
		}
		
		new WorldSynthCore(Commons.getExecutionDirectory(), injectedModuleRegisters);
		
		new WorldExtentManager();
		new WorldSynthStatTracker();
		new StageManager();
		
		/////////////////////////////////////////////////////////////////////////////////
		
		WorldSynthPatcher.primaryStage = primaryStage;
		primaryStage.setTitle("WorldSynth Patcher - Version " + VERSION);
		Image stageIcon = new Image(getClass().getResourceAsStream("worldSynthIcon.png"));
		primaryStage.getIcons().add(stageIcon);
		
		StageManager.setPrimaryStage(primaryStage);
		
		Parent root = FXMLLoader.load(getClass().getResource("WorldSynthMainScene.fxml"));
		
		Scene scene = new Scene(root, 1500, 800);
		scene.addEventHandler(KeyEvent.ANY, e -> {
			Node tabContentNode =  WorldSynthEditorController.instance.editorTabPane.getSelectionModel().getSelectedItem().getContent();
			if(tabContentNode instanceof SynthEditorPane) {
				SynthEditorPane currentEditor = (SynthEditorPane) tabContentNode;
				currentEditor.getKeyboardListener().handle(e);
			}
		});
		
		stylesheet = getClass().getResource("DarkThemeBase.css").toExternalForm();
		scene.getStylesheets().add(stylesheet);
		primaryStage.setScene(scene);
		primaryStage.setMinWidth(800);
		primaryStage.setMinHeight(700);
		
		primaryStage.show();
		
		splash.dispose();
	}
}
