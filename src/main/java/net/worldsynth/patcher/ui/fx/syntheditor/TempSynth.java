/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
package net.worldsynth.patcher.ui.fx.syntheditor;

import java.util.ArrayList;

import net.worldsynth.modulewrapper.ModuleConnector;
import net.worldsynth.modulewrapper.ModuleWrapper;
import net.worldsynth.synth.Synth;
import net.worldsynth.synth.io.Element;

public class TempSynth extends Synth {
	
	public TempSynth(String name, ArrayList<ModuleWrapper> wrapperList, ArrayList<ModuleConnector> moduleConnectorList, Synth parrentSynth) {
		//Create this as temp synth and set wrapperlist and connectorlist, then reinstance.
		super(name);
		this.wrapperList = wrapperList;
		this.moduleConnectorList = moduleConnectorList;
		reinstance(parrentSynth);
		
		recenterSynth();
	}
	
	public TempSynth(Element synthElement, Synth parrentSynth) {
		super("temp");
		fromElement(synthElement, parrentSynth);
		recenterSynth();
	}
	
	private void recenterSynth() {
		//Average positions and find the averaged center of the blueprint
		float x = 0;
		float y = 0;
		
		for(ModuleWrapper d: getWrapperList()) {
			x += d.posX;
			y += d.posY;
		}
		x /= getWrapperList().size();
		y /= getWrapperList().size();
		
		//Reidentify wrappers from the synth to avoid id conflictions and reposition
		for(ModuleWrapper d: getWrapperList()) {
			d.posX = d.posX - x;
			d.posY = d.posY - y;
		}
	}
	
	public void reinstance(Synth parrentSynth) {
		fromElement(toElement(), parrentSynth);
		for(ModuleWrapper d: wrapperList) {
			d.reidentify();
		}
	}
	
	public void setCenterTo(double x, double y) {
		recenterSynth();
		
		for(ModuleWrapper d: getWrapperList()) {
			d.posX = d.posX + x;
			d.posY = d.posY + y;
		}
	}
}
