/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
package net.worldsynth.patcher.build;

import java.util.concurrent.atomic.AtomicReference;

import javafx.application.Platform;
import javafx.beans.property.ReadOnlyObjectProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.concurrent.Task;
import net.worldsynth.common.WorldSynthCore;
import net.worldsynth.datatype.AbstractDatatype;
import net.worldsynth.module.ModuleOutputRequest;
import net.worldsynth.modulewrapper.ModuleWrapper;
import net.worldsynth.synth.Synth;
import net.worldsynth.util.event.build.BuildStatus;
import net.worldsynth.util.event.build.BuildStatusEvent;
import net.worldsynth.util.event.build.BuildStatusListener;

public class BuildTask extends Task<AbstractDatatype> {
	
	/**
	 * Used to send message updates in a thread-safe manner from the subclass to the
	 * FX application thread. AtomicReference is used so as to coalesce updates such
	 * that we don't flood the event queue.
	 */
	private AtomicReference<BuildStatus> statusUpdate = new AtomicReference<>();
	
	/**
	 * This is used so we have a thread-safe way to ask whether the task was started
	 * in the checkThread() method.
	 */
	private volatile boolean started = false;
	
	private final Synth synth;
	private final ModuleWrapper device;
	private final ModuleOutputRequest request;
	private final BuildStatusListener buildListener;
	
	public BuildTask(Synth synth, ModuleWrapper device, ModuleOutputRequest request, BuildStatusListener buildListener) {
		this.synth = synth;
		this.device = device;
		this.request = request;
		this.buildListener = buildListener;
		
		statusProperty().addListener(new ChangeListener<BuildStatus>() {

			@Override
			public void changed(ObservableValue<? extends BuildStatus> observable, BuildStatus oldValue, BuildStatus newValue) {
				if(BuildTask.this.buildListener != null) {
					BuildTask.this.buildListener.buildUpdate(new BuildStatusEvent(newValue, this));
				}
			}
		});
		
		setOnFailed(e -> {
			System.out.println("Build failed");
			getException().printStackTrace();
		});
	}
	
	@Override
	protected AbstractDatatype call() throws Exception {
		started = true;
		// TODO uese isCancelled() to stop a build in progress;
		BuildStatusListener l = new BuildStatusListener() {
			
			@Override
			public void buildUpdate(BuildStatusEvent event) {
				updateStatus(event.getStatus()); 
			}
		};
		
		AbstractDatatype data = WorldSynthCore.getModuleOutput(synth, device, request, l);
		if (isCancelled()) {
			return null;
		}
		return data;
	}
	
	private final SimpleObjectProperty<BuildStatus> status = new SimpleObjectProperty<BuildStatus>(this, "status", null);
	
	public final BuildStatus getStatus() {
		checkThread();
		return status.get();
	}
	
	public final ReadOnlyObjectProperty<BuildStatus> statusProperty() {
		checkThread();
		return status;
	}
	
	/**
	 * Updates the <code>status</code> property. Calls to updateStatus are coalesced
	 * and run later on the FX application thread, so calls to updateStatus, even
	 * from the FX Application thread, may not necessarily result in immediate
	 * updates to this property, and intermediate message values may be coalesced to
	 * save on event notifications.
	 * <p>
	 * <em>This method is safe to be called from any thread.</em>
	 * </p>
	 *
	 * @param status
	 *            the new build status
	 */
	protected void updateStatus(BuildStatus status) {
		if (Platform.isFxApplicationThread()) {
			this.status.set(status);
		} else {
			// It might be that the background thread
			// will update this status quite frequently, and we need
			// to throttle the updates so as not to completely clobber
			// the event dispatching system.
			if (statusUpdate.getAndSet(status) == null) {
				Platform.runLater(new Runnable() {
					@Override
					public void run() {
						final BuildStatus status = statusUpdate.getAndSet(null);
						BuildTask.this.status.set(status);
					}
				});
			}
		}
	}
	
	private void checkThread() {
		if (started && !Platform.isFxApplicationThread()) {
			throw new IllegalStateException("Task must only be used from the FX Application Thread");
		}
	}
}
