/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
package net.worldsynth.standalone.ui.stage;

import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.ToggleButton;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.KeyCode;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Pane;
import javafx.scene.layout.Priority;
import net.worldsynth.modulewrapper.ModuleWrapper;
import net.worldsynth.patcher.ui.fx.WorldSynthPatcher;
import net.worldsynth.util.event.module.ModuleApplyParametersEvent;

public class ModuleParametersStage extends PinnableUtilityStage {
	
	private Button applyButton;
	
	EventHandler<ModuleApplyParametersEvent> moduleApplyHandler;

	public ModuleParametersStage(ModuleWrapper device) {
		super();
		
		//Setup window
		setTitle(device.module.getModuleName());
		BorderPane rootPane = new BorderPane();
		
		//Pin button
		Image pinIcon = new Image(getClass().getResourceAsStream("pin16x16_white.png"));
		ToggleButton pinButton = new ToggleButton(null, new ImageView(pinIcon));
		pinButton.setOnAction(e -> {
			setPinned(pinButton.isSelected());
		});
		
		Button cancelButton = new Button("Cancel");
		cancelButton.setOnAction(e -> {
			close();
		});
		
		applyButton = new Button("Apply");
		applyButton.setOnAction(e -> {
			if(moduleApplyHandler != null) {
				moduleApplyHandler.handle(null);
			}
		});
		
		Button okButton = new Button("OK");
		okButton.setOnAction(e -> {
			close();
			if(moduleApplyHandler != null) {
				moduleApplyHandler.handle(null);
			}
		});
		
		HBox buttonsPane = new HBox();
		buttonsPane.setPadding(new Insets(0.0, 10.0, 5.0, 10.0));
		buttonsPane.getChildren().add(pinButton);
		
		Pane spacerPane = new Pane();
		HBox.setHgrow(spacerPane, Priority.SOMETIMES);
		buttonsPane.getChildren().add(spacerPane);
		
		buttonsPane.getChildren().add(cancelButton);
		buttonsPane.getChildren().add(applyButton);
		buttonsPane.getChildren().add(okButton);
		
		GridPane parameterPane = new GridPane();
		parameterPane.setPadding(new Insets(10.0));
		parameterPane.setHgap(2.0);
		parameterPane.setVgap(5.0);
		moduleApplyHandler = device.module.getModuleUI(parameterPane);
		
		//Setup keybinds
		parameterPane.setOnKeyReleased(e -> {
			if(e.getCode() == KeyCode.ENTER) {
				if(moduleApplyHandler != null) {
					moduleApplyHandler.handle(null);
				}
			}
			else if(e.getCode() == KeyCode.ESCAPE) {
				close();
			}
		});
		
		rootPane.setCenter(parameterPane);
		rootPane.setBottom(buttonsPane);
		
		Scene scene = new Scene(rootPane, rootPane.getPrefWidth(), rootPane.getPrefHeight());
		scene.getStylesheets().add(WorldSynthPatcher.stylesheet);
		setScene(scene);
		sizeToScene();
		show();
	}
}
