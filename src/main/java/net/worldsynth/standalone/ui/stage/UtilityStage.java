/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
package net.worldsynth.standalone.ui.stage;

import javafx.event.EventHandler;
import javafx.scene.image.Image;
import javafx.stage.Stage;
import javafx.stage.StageStyle;
import javafx.stage.WindowEvent;

public class UtilityStage extends Stage {
	
	private boolean hidden = false;
	
	private double x, y;
	private EventHandler<WindowEvent> windowClosingHandler = null;
	
	public UtilityStage() {
		Image stageIcon = new Image(getClass().getResourceAsStream("worldSynthIcon.png"));
		getIcons().add(stageIcon);
		
		initStyle(StageStyle.UTILITY);
		setResizable(false);
		setAlwaysOnTop(true);
		
		Stage primaryStage = StageManager.getPrimaryStage();
		x = primaryStage.getX() + primaryStage.getWidth() / 2.0;
		y = primaryStage.getY() + primaryStage.getHeight() / 2.0;
		
		setOnShowing(e -> {
			if(!hidden) {
				StageManager.registerOpenedUtilStage(this);
			}
		});
		
		setOnShown(e -> {
			setX(x - getWidth()/2.0);
			setY(y - getHeight()/2.0);
		});
		
		setOnHiding(e -> {
			x = getX() + getWidth()/2.0;
			y = getY() + getHeight()/2.0;
		});
		
		setOnCloseRequest(e -> {
			StageManager.registerClosedUtilStage(this);
			if(windowClosingHandler != null) {
				windowClosingHandler.handle(e);
			}
		});
	}
	
	public void setHidden(boolean hide) {
		if(hidden != hide) {
			if(hide) {
				//Hide window
				hidden = true;
				hide();
			}
			else {
				//Show window
				show();
				hidden = false;
			}
		}
	}
	
	@Override
	public void close() {
		super.close();
		StageManager.registerClosedUtilStage(this);
		if(windowClosingHandler != null) {
			windowClosingHandler.handle(new WindowEvent(this, null));
		}
	}

	public boolean isHidden() {
		return hidden;
	}
	
	public void setUtilityStageClosingHandler(EventHandler<WindowEvent> windowClosingHandler) {
		this.windowClosingHandler = windowClosingHandler;
	}
}
