/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
package net.worldsynth.standalone.ui.stage;

import java.util.ArrayList;
import java.util.concurrent.ExecutionException;

import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.concurrent.Task;
import javafx.event.EventHandler;
import javafx.scene.Scene;
import javafx.stage.Stage;
import net.worldsynth.common.Commons;
import net.worldsynth.patcher.ui.fx.WorldSynthPatcher;
import net.worldsynth.standalone.ui.stage.UiWindowUtil;
import net.worldsynth.standalone.ui.stage.UiWindowUtil.WindowRequestEvent;
import net.worldsynth.standalone.ui.stage.UiWindowUtil.WindowType;

/**
 * The stage manager helps to manage utility stages in a WorldSYnth Patcher instance and is responsible
 * for crating and keeping track of utility stages and their visibility any time.<br>
 * Some of the functionality provided by the stage manager is currently only enabled on selected systems
 * as other OS flavours have either been shown to behave differently in ways that break the some of the
 * functionality or have not been properly accounted for trough testing.
 * <br><br>
 * 
 * Currently state of the focus visibility management in the stage manager with different systems:<br>
 * 
 * <ul>
 * <b>Windows</b>: <i>Enabled</i>
 * <ul>
 * 04/02/2019 <b>Windows 10</b>:<br>
 * Windows 10 is tested to be working with this stage manager.
 * <br><br>
 * 04/02/2019 <b>Windows 8.1</b>:<br>
 * Windows 8.1 is tested to be working with this stage manager.
 * </ul>
 * </ul>
 * <br>
 * 
 * <ul>
 * <b>Linux</b>: <i>Disabled</i>
 * <ul>04/02/2019 <b>Ubuntu 18.04</b>: <br>
 * Ubuntu 18.04 has a behaviour where moving a window removes the focus until the window is released
 * from the moving action, and the manager can therefore not rely on window focus to figure whether
 * the user is interacting with a managed stage part of the current WorldSynth Patcher instance when
 * stages are being moved by the user.
 * </ul>
 * </ul>
 * <br>
 * 
 * 
 * <ul>
 * <b>Mac</b>: <i>Disabled</i><br>
 * No Mac systems are not accounted for due to missing proper test results.
 * </ul>
 * <br>
 * 
 * 
 * <ul>
 * <b>Solaris</b>: <i>Diasabled</i><br>
 * No Solaris systems are accounted for as WorldSynth Patcher is not intended to support Solaris environments.
 * </ul>
 * <br><br>
 * 
 * Results updated 04/02/2019
 * (dd/mm/yyyy)
 */
public class StageManager {
	@SuppressWarnings("unused")
	private static UiWindowUtil uiUtil;
	
	private static Stage primaryStage;
	private static ArrayList<UtilityStage> utilStageList = new ArrayList<UtilityStage>();
	
	public StageManager() {
		uiUtil = new UiWindowUtil(new EventHandler<UiWindowUtil.WindowRequestEvent>() {
			
			@Override
			public void handle(WindowRequestEvent event) {
				if(event.getWindowType() == WindowType.UTILITY) {
					UtilityStage newUtilityStage = new UtilityStage();
					newUtilityStage.setTitle(event.getWindowTitle());
					newUtilityStage.setResizable(event.getWindowResizable());
					newUtilityStage.setOnCloseRequest(event.getWindowClosingHandler());
					
					Scene utilityScen = new Scene(event.getContentPane());
					utilityScen.getStylesheets().add(WorldSynthPatcher.stylesheet);
					newUtilityStage.setScene(utilityScen);
					newUtilityStage.sizeToScene();
					newUtilityStage.show();
				}
				else if(event.getWindowType() == WindowType.UTILITY_PINNABLE) {
					PinnableUtilityStage newPinnableUtilityStage = new PinnableUtilityStage();
					newPinnableUtilityStage.setTitle(event.getWindowTitle());
					newPinnableUtilityStage.setResizable(event.getWindowResizable());
					newPinnableUtilityStage.setUtilityStageClosingHandler(event.getWindowClosingHandler());
					if(event.getWindowPinningProperty() != null) {
						event.getWindowPinningProperty().addListener((ObservableValue<? extends Boolean> observable, Boolean oldValue, Boolean newValue) -> {
							newPinnableUtilityStage.setPinned(newValue);
						});
					}
					
					Scene utilityScen = new Scene(event.getContentPane());
					utilityScen.getStylesheets().add(WorldSynthPatcher.stylesheet);
					newPinnableUtilityStage.setScene(utilityScen);
					newPinnableUtilityStage.sizeToScene();
					newPinnableUtilityStage.show();
				}
			}
		});
	}
	
	public static void lostFocusEventManager() {
		Task<Boolean> focusTask = new Task<Boolean>() {
			@Override
			protected Boolean call() throws Exception {
				Thread.sleep(10);
				if(WorldSynthPatcher.primaryStage.isFocused()) {
					return true;
				}
				for(UtilityStage s: utilStageList) {
					if(s.isFocused()) {
						return true;
					}
				}
				return false;
			}
		};
		focusTask.setOnSucceeded(e -> {
			try {
				boolean focusInApplication = focusTask.get();
				if(!focusInApplication) {
					hideUtilityStages(false);
				}
			} catch (InterruptedException | ExecutionException e1) {
				e1.printStackTrace();
			}
		});
		new Thread(focusTask).start();
	}
	
	public static void setPrimaryStage(Stage stage) {
		primaryStage = stage;
		
		//Only enable focus manager on windows systems
		if(Commons.systemIsWindows()) {
			stage.focusedProperty().addListener(new ChangeListener<Boolean>() {
				@Override
				public void changed(ObservableValue<? extends Boolean> observable, Boolean oldValue, Boolean newValue) {
					if(newValue == false) {
						lostFocusEventManager();
					}
					else {
						showUtilityStages();
					}
				}
			});
		}
		
		primaryStage.iconifiedProperty().addListener(new ChangeListener<Boolean>() {
			@Override
			public void changed(ObservableValue<? extends Boolean> observable, Boolean oldValue, Boolean newValue) {
				if(newValue) {
					hideUtilityStages(true);
				}
				else {
					showUtilityStages();
				}
			}
		});
		
		primaryStage.setOnCloseRequest(e -> {
			StageManager.closeUtilityStages();
		});
	}
	
	public static Stage getPrimaryStage() {
		return primaryStage;
	}
	
	public static void registerOpenedUtilStage(UtilityStage utilStage) {
		utilStageList.add(utilStage);
		if(!Commons.systemIsWindows()) return;
		
		utilStage.focusedProperty().addListener(new ChangeListener<Boolean>() {
			@Override
			public void changed(ObservableValue<? extends Boolean> observable, Boolean oldValue, Boolean newValue) {
				if(newValue == false) {
					lostFocusEventManager();
				}
			}
		});
	}

	public static void registerClosedUtilStage(UtilityStage utilStage) {
		utilStageList.remove(utilStage);
	}

	private static void hideUtilityStages(boolean hidePinned) {
		for(UtilityStage s: utilStageList) {
			if(hidePinned) {
				s.setHidden(true);
			}
			else {
				if(s instanceof PinnableUtilityStage) {
					boolean pinned = ((PinnableUtilityStage) s).isPinned();
					if(!pinned){
						s.setHidden(true);
					}
				}
				else {
					s.setHidden(true);
				}
			}
		}
	}
	
	private static void showUtilityStages() {
		for(UtilityStage s: utilStageList) {
			if(s.isHidden()) {
				s.setHidden(false);
			}
		}
	}
	
	private static void closeUtilityStages() {
		while(utilStageList.size() > 0) {
			utilStageList.get(0).close();
		}
	}
}
