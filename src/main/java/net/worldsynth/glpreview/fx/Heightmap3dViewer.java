/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
package net.worldsynth.glpreview.fx;

import javafx.embed.swing.SwingNode;
import javafx.scene.layout.BorderPane;
import net.worldsynth.glpreview.heightmap.Heightmap3DGLPreviewBufferedGL3;

public class Heightmap3dViewer extends BorderPane {
	
	private float[][] colorscale = {{0, 0, 0, 1},
			{0.2f, 1.0f, 1.0f, 0.0f},
			{0.5f, 0.0f, 1.0f, 0.0f},
			{0.7f, 0.75f, 0.56f, 0.2f},
			{0.8f, 0.5f, 0.5f, 0.5f},
			{1, 1, 1, 1}};
	
	private final Heightmap3DGLPreviewBufferedGL3 openGL3DRenderer;
	private final SwingNode swingNode = new SwingNode();
	
	public Heightmap3dViewer() {
		openGL3DRenderer = new Heightmap3DGLPreviewBufferedGL3();
		swingNode.setContent(openGL3DRenderer);
		setCenter(swingNode);
	}
	
	public void setHeightmap(float[][] heightmap, float maxHeightUnit, double size) {
		openGL3DRenderer.setHeightmap(heightmap, maxHeightUnit, size);
	}
	
	public void setColorscale(float[][] colorscale) {
		this.colorscale = colorscale;
		openGL3DRenderer.setColorscale(this.colorscale);
	}
}
