/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
package net.worldsynth.glpreview.fx;

import javafx.embed.swing.SwingNode;
import javafx.scene.layout.BorderPane;
import net.worldsynth.glpreview.voxel.Blockspace3DGLPreviewBufferedGL3;

public class Blockspace3dViewer extends BorderPane {
	
	private final Blockspace3DGLPreviewBufferedGL3 openGL3DRenderer;
	private final SwingNode swingNode = new SwingNode();
	
	public Blockspace3dViewer() {
		openGL3DRenderer = new Blockspace3DGLPreviewBufferedGL3();
		swingNode.setContent(openGL3DRenderer);
		setCenter(swingNode);
	}
	
	public void setBlockspace(int[][][] blockspaceMaterialId) {
		openGL3DRenderer.setBlockspace(blockspaceMaterialId);
	}
}
