/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
package net.worldsynth.glpreview.voxel;

import com.jogamp.opengl.GLCapabilities;
import com.jogamp.opengl.GLProfile;

import net.worldsynth.glpreview.buffered.BufferedGL3Panel;

public class Blockspace3DGLPreviewBufferedGL3 extends BufferedGL3Panel {
	private static final long serialVersionUID = -1895542307473079689L;
	
	private VoxelModel voxelModel;
	
	public Blockspace3DGLPreviewBufferedGL3() {
		GLProfile glprofile = GLProfile.get(GLProfile.GL3);
		GLCapabilities glcapabilities = new GLCapabilities(glprofile);
		setRequestedGLCapabilities(glcapabilities);
	}
	
	public void setBlockspace(int[][][] blockspaceMaterialId) {
		voxelModel = new VoxelModel(blockspaceMaterialId);
		
		startNewModel();
		loadModel(voxelModel);
		endNewModel();
		display();
	}
}
