/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
package net.worldsynth.glpreview.voxel;

import com.jogamp.opengl.GLCapabilities;
import com.jogamp.opengl.GLProfile;

import net.worldsynth.glpreview.Line;
import net.worldsynth.glpreview.buffered.BufferedGL3Panel;
import net.worldsynth.glpreview.model.AbstractLineModel;

public class Object3DGLPreviewBufferedGL3 extends BufferedGL3Panel {
	private static final long serialVersionUID = -1895542307473079689L;
	
	private VoxelModel voxelModel;
	
	public Object3DGLPreviewBufferedGL3() {
		GLProfile glprofile = GLProfile.get(GLProfile.GL3);
		GLCapabilities glcapabilities = new GLCapabilities(glprofile);
		setRequestedGLCapabilities(glcapabilities);
	}
	
	public void setObject(int[][] object, int width, int height, int length, int xMin, int yMin, int zMin, int xMax, int yMax, int zMax) {
		int[][][] blockspaceMaterialId = new int[width][height][length];
		for(int[] block: object) {
			blockspaceMaterialId[block[0] - xMin][block[1] - yMin][block[2] - zMin] = block[3];
		}
		
//		voxelModel = new VoxelModel(blockspaceMaterialId);
		float xOffset = (float) Math.ceil(width / 2.0) - (float) xMax - 0.5f;
		float yOffset = (float) Math.ceil(height / 2.0) - (float) yMax - 0.5f;
		float zOffset = (float) Math.ceil(length / 2.0) - (float) zMax - 0.5f;
		voxelModel = new VoxelModel(blockspaceMaterialId, xOffset, yOffset, zOffset);
		
		startNewModel();
		if(voxelModel.getPrimitivesCount() > 0) {
			loadModel(voxelModel);
		}
		loadModel(new CrosshairModel());
		endNewModel();
		display();
	}
	
	private class CrosshairModel extends AbstractLineModel {
		
		public CrosshairModel() {
			initVertexArray(3);
			
			Line line = new Line();
			line.setVertex(0, -100, 0, 0);
			line.setVertex(1, 100, 0, 0);
			line.setColor(1.0f, 0.0f, 0.0f);
			insertVertexArray(line, 0);
			
			line.setVertex(0, 0, -100, 0);
			line.setVertex(1, 0, 100, 0);
			line.setColor(0.0f, 1.0f, 0.0f);
			insertVertexArray(line, 1);
			
			line.setVertex(0, 0, 0, -100);
			line.setVertex(1, 0, 0, 100);
			line.setColor(0.0f, 0.0f, 1.0f);
			insertVertexArray(line, 2);
		}

		@Override
		public int getPrimitivesCount() {
			return 3;
		}
	}
}
