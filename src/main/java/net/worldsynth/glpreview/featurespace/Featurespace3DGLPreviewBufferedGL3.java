/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
package net.worldsynth.glpreview.featurespace;

import com.jogamp.opengl.GLCapabilities;
import com.jogamp.opengl.GLProfile;

import net.worldsynth.glpreview.Line;
import net.worldsynth.glpreview.buffered.BufferedGL3Panel;
import net.worldsynth.glpreview.model.AbstractLineModel;

public class Featurespace3DGLPreviewBufferedGL3 extends BufferedGL3Panel {
	private static final long serialVersionUID = -2511868586935099949L;
	
	private FeaturespaceModel featurespaceModel;
	
	public Featurespace3DGLPreviewBufferedGL3() {
		GLProfile glprofile = GLProfile.get(GLProfile.GL3);
		GLCapabilities glcapabilities = new GLCapabilities(glprofile);
		setRequestedGLCapabilities(glcapabilities);
	}
	
	public void setFeaturespace(double[][] points, double x, double y, double z, double width, double height, double length) {
		featurespaceModel = new FeaturespaceModel(points, x+width/2d, y+height/2d, z+length/2d, width, height, length);
		
		startNewModel();
		loadModel(featurespaceModel);
		loadModel(new WireframeBoxModel((float) x, (float) y, (float) z, (float) width, (float) height, (float) length));
		endNewModel();
		display();
	}
	
	private class WireframeBoxModel extends AbstractLineModel {
		
		public WireframeBoxModel(float x, float y, float z, float width, float height, float length) {
			initVertexArray(3);
			
			Line line = new Line();
			line.setVertex(0, -width/2f, -height/2f, -length/2f);
			line.setVertex(1, width/2f, -height/2f, -length/2f);
			line.setColor(1.0f, 0.0f, 0.0f);
			insertVertexArray(line, 0);
			
			line.setVertex(0, -width/2f, -height/2f, -length/2f);
			line.setVertex(1, -width/2f, height/2f, -length/2f);
			line.setColor(0.0f, 1.0f, 0.0f);
			insertVertexArray(line, 1);
			
			line.setVertex(0, -width/2f, -height/2f, -length/2f);
			line.setVertex(1, -width/2f, -height/2f, length/2f);
			line.setColor(0.0f, 0.0f, 1.0f);
			insertVertexArray(line, 2);
		}

		@Override
		public int getPrimitivesCount() {
			return 3;
		}
	}
}