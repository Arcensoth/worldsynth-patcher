/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
package net.worldsynth.glpreview.buffered;

import com.jogamp.opengl.GL3;

public class BufferedModel {
	private Program shaderProgram;
	private int GL3DrawMode;
	private VAO vao;
	private int vertexCount;
	
	public BufferedModel(VAO vao, int GL3DrawMode, Program shaderProgram, int vertexCount) {
		this.shaderProgram = shaderProgram;
		this.GL3DrawMode = GL3DrawMode;
		this.vao = vao;
		this.vertexCount = vertexCount;
	}
	
	public int getVaoId() {
		return vao.getVaoId();
	}
	
	public int getVertexCount() {
		return vertexCount;
	}
	
	public Program getShaderProgram() {
		return shaderProgram;
	}
	
	public void drawModel(GL3 gl3) {
		if(vao == null) return;
		
		gl3.glBindVertexArray(getVaoId());
		vao.enableVertexAttribArrays(gl3);
		gl3.glDrawArrays(GL3DrawMode, 0, getVertexCount());
		vao.disableVertexAttribArrays(gl3);
		gl3.glBindVertexArray(0);
	}
	
	public void decommission(GL3 gl3) {
		////Remove the associated VAO from buffer and remove the stored information and reference to it
		vao.decommission(gl3);
		vao = null;
		vertexCount = 0;
	}
}
