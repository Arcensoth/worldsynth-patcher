/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
package net.worldsynth.glpreview.buffered;

import java.nio.FloatBuffer;
import java.util.ArrayList;

import com.jogamp.common.nio.Buffers;
import com.jogamp.opengl.GL3;

final class VAO {
	
	private int vaoId;
	private ArrayList<Integer> vbos = new ArrayList<Integer>();
	private ArrayList<Integer> attributes = new ArrayList<Integer>();
	
	VAO(GL3 gl3) {
		int[] idArray = new int[1];
		gl3.glGenVertexArrays(1, idArray, 0);
		vaoId = idArray[0];
	}
	
	public int getVaoId() {
		return vaoId;
	}
	
	public void bind(GL3 gl3) {
		gl3.glBindVertexArray(vaoId);
	}
	
	public void unbind(GL3 gl3) {
		gl3.glBindVertexArray(0);
	}
	
	public void storeDataInAttributeList(GL3 gl3, int attributeNumber, int size, float[] data) {
		if(vaoId == 0) return;
		//Bind this vao
		bind(gl3);
		//Create a new vertex buffer object
		int vboId = createBuffer(gl3);
		//Bind the new vertex buffer object
		gl3.glBindBuffer(GL3.GL_ARRAY_BUFFER, vboId);
		//Buffer the vertex array
		gl3.glBufferData(GL3.GL_ARRAY_BUFFER, (long) data.length * (long) (Float.SIZE/8), createBufferFromData(data), GL3.GL_STATIC_DRAW);
		//Setup attribute pointer and store attribute for later use
		gl3.glVertexAttribPointer(attributeNumber, size, GL3.GL_FLOAT, false, 0, 0);
		attributes.add(attributeNumber);
		//Unbind the vertex buffer object
		gl3.glBindBuffer(GL3.GL_ARRAY_BUFFER, 0);
		//Unbind this vao
		unbind(gl3);
	}
	
	public void enableVertexAttribArrays(GL3 gl3) {
		for(int attribute: attributes) {
			gl3.glEnableVertexAttribArray(attribute);
		}
	}
	
	public void disableVertexAttribArrays(GL3 gl3) {
		for(int attribute: attributes) {
			gl3.glDisableVertexAttribArray(attribute);
		}
	}
	
	public void decommission(GL3 gl3) {
		//Delete all vbos associated with this vao
		for(int vbo: vbos) {
			int[] idArray = {vbo};
			gl3.glDeleteBuffers(1, idArray, 0);
		}
		vbos = new ArrayList<Integer>();
		
		//Delete this vao
		int[] idArray = {vaoId};
		gl3.glDeleteVertexArrays(1, idArray, 0);
		vaoId = 0;
	}
	
	private int createBuffer(GL3 gl3) {
		int[] idArray = new int[1];
		gl3.glGenBuffers(1, idArray , 0);
		vbos.add(idArray[0]);
		return idArray[0];
	}
	
	private FloatBuffer createBufferFromData(float[] data) {
		FloatBuffer buffer = Buffers.newDirectFloatBuffer(data);
		return buffer;
	}
}
