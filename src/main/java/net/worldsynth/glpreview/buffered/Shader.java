/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
package net.worldsynth.glpreview.buffered;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;

import com.jogamp.opengl.GL3;

public class Shader {
	
	private int shaderId;
	
	public Shader(GL3 gl, String fileName, ShaderType type){
		// load the source from inside the package
		String shaderSource = loadStringFileFromCurrentPackage( fileName);
		// define the shaper type from the enum
		int shaderType = type==ShaderType.VertexShader?GL3.GL_VERTEX_SHADER:GL3.GL_FRAGMENT_SHADER;
		// create the shader id
		shaderId = gl.glCreateShader(shaderType);
		//  link the id and the source
		gl.glShaderSource(shaderId, 1, new String[] { shaderSource }, null);
		//compile the shader
		gl.glCompileShader(shaderId);
		
		//Print out the log
		String log = getShaderInfoLog(gl);
		if(log.equals("")) {
			System.out.println("Compiled shader \"" + fileName + "\" successfully");
		}
		else {
			System.out.println("Problems with shader \"" + fileName + "\", printing log");
			System.out.println(log);
		}
		
	}
	
	private String loadStringFileFromCurrentPackage( String fileName){
		InputStream stream = this.getClass().getResourceAsStream(fileName);

		BufferedReader reader = new BufferedReader(new InputStreamReader(stream));
		// allocate a string builder to add line per line 
		StringBuilder strBuilder = new StringBuilder();

		try {
			String line = reader.readLine();
			// get text from file, line per line
			while(line != null){
				strBuilder.append(line + "\n");
				line = reader.readLine();	
			}
			// close resources
			reader.close();
			stream.close();
		} catch (IOException e) {
			e.printStackTrace();
		}

		return strBuilder.toString();
	}
	
	public String getShaderInfoLog(GL3 gl) {
		//Get the shader log length
		final int logLen = getShaderLogLength(gl);
		if (logLen <= 0)
			return "";

		//If there is content in the log, get and return it
		final int[] retLength = new int[1];
		final byte[] bytes = new byte[logLen + 1];
		gl.glGetShaderInfoLog(shaderId, logLen, retLength, 0, bytes, 0);
		final String logMessage = new String(bytes);

		return String.format("ShaderLog: %s", logMessage);
	}
	
	private int getShaderLogLength(GL3 gl) {
		final int params[] = new int[1];
		gl.glGetShaderiv(shaderId, GL3.GL_INFO_LOG_LENGTH, params, 0);
		return params[0];
	}
	
	public int getShaderId() {
		return shaderId;
	}
	
	public void decommissionShader(GL3 gl) {
		gl.glDeleteShader(shaderId);
	}
	
	public enum ShaderType{
		VertexShader, FragmentShader
	}
}
