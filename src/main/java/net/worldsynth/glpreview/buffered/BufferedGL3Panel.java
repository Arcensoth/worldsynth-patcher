/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
package net.worldsynth.glpreview.buffered;

import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.MouseMotionListener;
import java.awt.event.MouseWheelEvent;
import java.awt.event.MouseWheelListener;
import java.util.ArrayList;

import javax.swing.SwingUtilities;

import com.jogamp.opengl.GL3;
import com.jogamp.opengl.GLAutoDrawable;
import com.jogamp.opengl.GLCapabilities;
import com.jogamp.opengl.GLEventListener;
import com.jogamp.opengl.GLProfile;
import com.jogamp.opengl.awt.GLJPanel;

import net.worldsynth.glpreview.buffered.Shader.ShaderType;
import net.worldsynth.glpreview.model.AbstractModel;

public class BufferedGL3Panel extends GLJPanel implements GLEventListener, MouseMotionListener, MouseListener, MouseWheelListener {
	private static final long serialVersionUID = -4627683443751384368L;

	// Shader programs
	private Program surfaceShaderProgram;
	private Program lineShaderProgram;
	private Program pointShaderProgram;

	// Vertex Attribute Locations
//	private int vertexLoc, colorLoc, normalLoc;
	
	// storage for Matrices
	float projMatrix[] = new float[16];
	float viewMatrix[] = new float[16];
	
	private float[] lightPos = {500.0f, 500.0f, 500.0f};
	private float lightIntensity = 1.5f;
	
	private Loader loader = new Loader();
	private ArrayList<BufferedModel> currentBufferedModels = new ArrayList<BufferedModel>();
	private ArrayList<BufferedModel> newBufferedModels = new ArrayList<BufferedModel>();
	
	private final int NO_MODELS = 0;
	private final int NEW_MODELS_BUFFERING = 1;
	private final int NEW_MODELS_READY = 2;
	private final int MODELS_READY = 3;
	
	private int modelsState = NO_MODELS;
	
	private volatile ArrayList<AbstractModel<?>> modelsToBuffer = new ArrayList<AbstractModel<?>>();
	
	
	private float lastMouseX;
	private float lastMouseY;
	
	private float pitch = 120.0f;
	private float yaw = 0.0f;
	private float zoom = -4.0f;
	private float yLookatHeight = 0.0f;
	
	
	public BufferedGL3Panel() {
		GLProfile glprofile = GLProfile.get(GLProfile.GL3);
		GLCapabilities glcapabilities = new GLCapabilities(glprofile);
		setRequestedGLCapabilities(glcapabilities);
		
		addGLEventListener(this);
		
		addMouseMotionListener(this);
		addMouseListener(this);
		addMouseWheelListener(this);
	}
	
	protected void startNewModel() {
		modelsToBuffer.clear();
		modelsState = NEW_MODELS_BUFFERING;
	}
	
	protected void endNewModel() {
		modelsState = NEW_MODELS_READY;
	}
	
	protected void loadModel(AbstractModel<?> model) throws IllegalArgumentException {
		if(model.getPrimitivesCount() == 0) {
			throw new IllegalArgumentException("Model has no primitives, primitives array has lenght 0.");
		}
		
		modelsToBuffer.add(model);
	}
	
	@Override
	public void reshape(GLAutoDrawable drawable, int x, int y, int width, int height) {
		float ratio;
		// Prevent a divide by zero, when window is too short
		// (you can't make a window of zero width).
		if (height == 0)
			height = 1;

		ratio = (1.0f * width) / height;
		this.projMatrix = Commons.buildProjectionMatrix(53.13f, ratio, 1.0f, 3000.0f, this.projMatrix);
	}

	@Override
	public void init(GLAutoDrawable drawable) {
		//Setup everything that needs to be set up in the openGL window
		GL3 gl3 = drawable.getGL().getGL3();
		
		gl3.glEnable(GL3.GL_DEPTH_TEST);
		gl3.glClearColor(0.0f, 0.0f, 0.0f, 1.0f);
		
//		gl3.glEnable(GL3.GL_CULL_FACE);
//		gl3.glCullFace(GL3.GL_BACK);
//		gl3.glFrontFace(GL3.GL_CCW);
		
		//Setup shader programs
		//Create and compile the vertex and fragment shaders and attach them to a programs
		Shader pointVertexShader = new Shader(gl3, "PointVertex.shader", ShaderType.VertexShader);
		Shader pointFragmentShader = new Shader(gl3, "PointFragment.shader", ShaderType.FragmentShader);
		pointShaderProgram = new Program(gl3, pointVertexShader, pointFragmentShader);
		
		Shader lineVertexShader = new Shader(gl3, "LineVertex.shader", ShaderType.VertexShader);
		Shader lineFragmentShader = new Shader(gl3, "LineFragment.shader", ShaderType.FragmentShader);
		lineShaderProgram = new Program(gl3, lineVertexShader, lineFragmentShader);
		
		Shader surfaceVertexShader = new Shader(gl3, "SurfaceVertex.shader", ShaderType.VertexShader);
		Shader surfaceFragmentShader = new Shader(gl3, "SurfaceFragment.shader", ShaderType.FragmentShader);
		surfaceShaderProgram = new Program(gl3, surfaceVertexShader, surfaceFragmentShader);
	}

	@Override
	public void dispose(GLAutoDrawable drawable) {
		GL3 gl3 = drawable.getGL().getGL3();
		loader.cleanUpAll(gl3);
		currentBufferedModels = null;
		newBufferedModels = null;
		
		//Decommission shader programs
		surfaceShaderProgram.decommissionProgram(gl3);
		surfaceShaderProgram = null;
		
		lineShaderProgram.decommissionProgram(gl3);
		lineShaderProgram = null;
		
		pointShaderProgram.decommissionProgram(gl3);
		pointShaderProgram = null;
	}

	@Override
	public void display(GLAutoDrawable drawable) {
		if(modelsState == NO_MODELS) {
			return;
		}
		
		GL3 gl3 = drawable.getGL().getGL3();
		
		//Load new models to buffer
		while(modelsToBuffer.size() > 0) {
			BufferedModel newBufferedModel = loader.loadModelToVAO(gl3, modelsToBuffer.get(0), pointShaderProgram, lineShaderProgram, surfaceShaderProgram);
			modelsToBuffer.remove(0);
			newBufferedModels.add(newBufferedModel);
		}
		
		if(modelsState == NEW_MODELS_READY) {
			loader.cleanUpModels(gl3, currentBufferedModels);
			currentBufferedModels = newBufferedModels;
			newBufferedModels = new ArrayList<BufferedModel>();
			modelsState = MODELS_READY;
		}
		
		
		gl3.glClear(GL3.GL_COLOR_BUFFER_BIT | GL3.GL_DEPTH_BUFFER_BIT);
		
		float dist = 100.0f;
		float cameraPosX = (float)Math.sin(Math.toRadians(pitch)) * (float)Math.cos(Math.toRadians(yaw)) * zoom * dist;
		float cameraPosY = (float)Math.cos(Math.toRadians(pitch)) * zoom * dist + yLookatHeight;
		float cameraPosZ = (float)Math.sin(Math.toRadians(pitch)) * (float)Math.sin(Math.toRadians(yaw)) * zoom * dist;
		float lookPosX = 0;
		float lookPosY = yLookatHeight;
		float lookPosZ = 0;
		
		Commons.setCamera(	cameraPosX, cameraPosY, cameraPosZ, 
							lookPosX, lookPosY, lookPosZ,
							this.viewMatrix);
		
		for(BufferedModel model: currentBufferedModels) {
			//Set correct shader for the model
			Program shaderProgram = model.getShaderProgram();
			gl3.glUseProgram(shaderProgram.getProgramId());
			
			//Get the location of the uniform variables
			int projMatrixLoc = gl3.glGetUniformLocation(shaderProgram.getProgramId(), "projMatrix");
			int viewMatrixLoc = gl3.glGetUniformLocation(shaderProgram.getProgramId(), "viewMatrix");
			int lightVectorLoc = gl3.glGetUniformLocation(shaderProgram.getProgramId(), "lightPosition");
			int lightIntensityLoc = gl3.glGetUniformLocation(shaderProgram.getProgramId(), "lightIntensity");
			
			//Set the view and the projection matrix
			gl3.glUniformMatrix4fv(projMatrixLoc, 1, false, this.projMatrix, 0);
			gl3.glUniformMatrix4fv(viewMatrixLoc, 1, false, this.viewMatrix, 0);
			gl3.glUniform4f(lightVectorLoc, lightPos[0], lightPos[1], lightPos[2], 1.0f);
			gl3.glUniform1f(lightIntensityLoc, lightIntensity);
			
			model.drawModel(gl3);
		}
		
		//Check out error
		int error = gl3.glGetError();
		if(error!=0) {
			System.err.println("ERROR on render : " + error);
		}
	}
	
	
//-------------------------------------- Interaction events --------------------------------------//
	
	
	@Override
	public void mouseMoved(MouseEvent e) {
		lastMouseX = e.getX();
		lastMouseY = e.getY();
	}
	
	@Override
	public void mouseDragged(MouseEvent e) {
		float x = e.getX();
		float y = e.getY();
		
		float w = getWidth();
		float h = getHeight();
		
		if(SwingUtilities.isLeftMouseButton(e)) {
			yaw += (x-lastMouseX)/w * 180;
			pitch += (y-lastMouseY)/h * 45;
			
			if(pitch <= 0.0f) {
				pitch = 0.1f;
			}
			else if(pitch >= 179.9f) {
				pitch = 179.9f;
			}
			display();
		}
		else if(SwingUtilities.isMiddleMouseButton(e)) {
			yLookatHeight += (y-lastMouseY) * -zoom * 0.2f;
			
			if(yLookatHeight <= -100.0f) {
				yLookatHeight = -100.0f;
			}
			else if(yLookatHeight >= 100.0f) {
				yLookatHeight = 100.0f;
			}
			display();
		}
		else if(SwingUtilities.isRightMouseButton(e)) {
			float lightDist = 5000;
			float mouseDistFromCenter = (float) (Math.sqrt(Math.pow(w/2 - x, 2) + Math.pow(h/2 - y, 2)) / Math.sqrt(Math.pow(w/2, 2) + Math.pow(h/2, 2)));
			float mouseXDistDfromCenter = (x - w/2) / (w/2);
			float mouseYDistDfromCenter = (y - h/2) / (h/2);
			
			float lightX = (float) (lightDist * Math.asin(mouseXDistDfromCenter));
			float lightY = (float) (lightDist * Math.acos(mouseDistFromCenter));
			float lightZ = (float) (lightDist * Math.asin(mouseYDistDfromCenter));
			
			//Rotate light positon acording to yaw
			double yawRad = Math.toRadians(yaw + 90);
			double rotatedLightX = lightX * Math.cos(yawRad) - lightZ * Math.sin(yawRad);
			double rotatedLightZ = lightX * Math.sin(yawRad) + lightZ * Math.cos(yawRad);
			
			lightPos = new float[]{(float)rotatedLightX, lightY, (float)rotatedLightZ};
			display();
		}
		
		lastMouseX = x;
		lastMouseY = y;
	}
	
	@Override
	public void mouseReleased(MouseEvent e) {}
	
	@Override
	public void mousePressed(MouseEvent e) {}
	
	@Override
	public void mouseExited(MouseEvent e) {}
	
	@Override
	public void mouseEntered(MouseEvent e) {}
	
	@Override
	public void mouseClicked(MouseEvent e) {
		if(SwingUtilities.isMiddleMouseButton(e)) {
			yLookatHeight = 0;
			display();
		}
	}
	
	@Override
	public void mouseWheelMoved(MouseWheelEvent e) {
		float maxZoom = -0.2f;
		float minZoom = -5.0f;
		
		zoom -= (float) e.getWheelRotation() / 5;
		if(zoom < minZoom) zoom = minZoom;
		else if(zoom > maxZoom) zoom = maxZoom;
		display();
	}
}
