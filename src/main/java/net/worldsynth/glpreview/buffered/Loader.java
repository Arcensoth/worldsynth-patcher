/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
package net.worldsynth.glpreview.buffered;

import java.util.ArrayList;

import com.jogamp.opengl.GL3;

import net.worldsynth.glpreview.PrimitiveType;
import net.worldsynth.glpreview.model.AbstractModel;

public class Loader {
	
	private ArrayList<BufferedModel> loadedModels = new ArrayList<BufferedModel>();
	
	public BufferedModel loadModelToVAO(GL3 gl3, AbstractModel<?> model, Program pointShaderProgram, Program lineShaderProgram, Program surfaceShaderProgram) {
		if(model.getPrimitivesType() == PrimitiveType.POINTS) {
			return loadPointModelToVAO(gl3, model, pointShaderProgram);
		}
		else if(model.getPrimitivesType() == PrimitiveType.LINES) {
			return loadLineModelToVAO(gl3, model, lineShaderProgram);
		}
		else if(model.getPrimitivesType() == PrimitiveType.TRIFACES) {
			return loadTrifaceModelToVAO(gl3, model, surfaceShaderProgram);
		}
		else if(model.getPrimitivesType() == PrimitiveType.QUADFACES) {
			return loadQuadfaceModelToVAO(gl3, model, surfaceShaderProgram);
		}
		else {
			return null;
		}
	}
	
	private BufferedModel loadPointModelToVAO(GL3 gl3, AbstractModel<?> model, Program pointShaderProgram) {
		//Get the attribute locations for vertex positions and colors
		int vertexLoc = pointShaderProgram.glGetAttribLocation(gl3, "in_position");
		int colorLoc = pointShaderProgram.glGetAttribLocation(gl3, "in_color");
		
		float[] vertexPositions = model.getVertexPositionArray();
		float[] vertexColors = model.getVertexColorArray();
		
		VAO vao = new VAO(gl3);
		vao.storeDataInAttributeList(gl3, vertexLoc, 4, vertexPositions);
		vao.storeDataInAttributeList(gl3, colorLoc, 4, vertexColors);
		BufferedModel newLoadedModel = new BufferedModel(vao, GL3.GL_POINTS, pointShaderProgram, vertexPositions.length/4);
		loadedModels.add(newLoadedModel);
		return newLoadedModel;
	}
	
	private BufferedModel loadLineModelToVAO(GL3 gl3, AbstractModel<?> model, Program lineShaderProgram) {
		//Get the attribute locations for vertex positions, colors and normals
		int vertexLoc = lineShaderProgram.glGetAttribLocation(gl3, "in_position");
		int colorLoc = lineShaderProgram.glGetAttribLocation(gl3, "in_color");
		
		float[] vertexPositions = model.getVertexPositionArray();
		float[] vertexColors = model.getVertexColorArray();
		
		VAO vao = new VAO(gl3);
		vao.storeDataInAttributeList(gl3, vertexLoc, 4, vertexPositions);
		vao.storeDataInAttributeList(gl3, colorLoc, 4, vertexColors);
		BufferedModel newLoadedModel = new BufferedModel(vao, GL3.GL_LINES, lineShaderProgram, vertexPositions.length/4);
		loadedModels.add(newLoadedModel);
		return newLoadedModel;
	}
	
	private BufferedModel loadTrifaceModelToVAO(GL3 gl3, AbstractModel<?> model, Program surfaceShaderProgram) {
		//Get the attribute locations for vertex positions, colors and normals
		int vertexLoc = surfaceShaderProgram.glGetAttribLocation(gl3, "in_position");
		int colorLoc = surfaceShaderProgram.glGetAttribLocation(gl3, "in_color");
		int normalLoc = surfaceShaderProgram.glGetAttribLocation(gl3, "in_normal");
		
		float[] vertexPositions = model.getVertexPositionArray();
		float[] vertexColors = model.getVertexColorArray();
		float[] vertexNormals = model.getVertexNormalArray();
		
		VAO vao = new VAO(gl3);
		vao.storeDataInAttributeList(gl3, vertexLoc, 4, vertexPositions);
		vao.storeDataInAttributeList(gl3, colorLoc, 4, vertexColors);
		vao.storeDataInAttributeList(gl3, normalLoc, 4, vertexNormals);
		BufferedModel newLoadedModel = new BufferedModel(vao, GL3.GL_TRIANGLES, surfaceShaderProgram, vertexPositions.length/4);
		loadedModels.add(newLoadedModel);
		return newLoadedModel;
	}
	
	private BufferedModel loadQuadfaceModelToVAO(GL3 gl3, AbstractModel<?> model, Program surfaceShaderProgram) {
		//Get the attribute locations for vertex positions, colors and normals
		int vertexLoc = surfaceShaderProgram.glGetAttribLocation(gl3, "in_position");
		int colorLoc = surfaceShaderProgram.glGetAttribLocation(gl3, "in_color");
		int normalLoc = surfaceShaderProgram.glGetAttribLocation(gl3, "in_normal");
		
		float[] vertexPositions = model.getVertexPositionArray();
		float[] vertexColors = model.getVertexColorArray();
		float[] vertexNormals = model.getVertexNormalArray();
		
		VAO vao = new VAO(gl3);
		vao.storeDataInAttributeList(gl3, vertexLoc, 4, vertexPositions);
		vao.storeDataInAttributeList(gl3, colorLoc, 4, vertexColors);
		vao.storeDataInAttributeList(gl3, normalLoc, 4, vertexNormals);
		BufferedModel newLoadedModel = new BufferedModel(vao, GL3.GL_QUADS, surfaceShaderProgram, vertexPositions.length/4);
		loadedModels.add(newLoadedModel);
		return newLoadedModel;
	}
	
	public void cleanUpAll(GL3 gl3) {
		//Remove all loaded models from buffer
		for(BufferedModel lModel: loadedModels) {
			lModel.decommission(gl3);
		}
		//Cleanup registry of loaded models
		loadedModels = new ArrayList<BufferedModel>();
	}
	
	public void cleanUpModels(GL3 gl3, ArrayList<BufferedModel> models) {
		//Remove all loaded models from buffer
		for(BufferedModel model: models) {
			model.decommission(gl3);
		}
		//Cleanup registry removing from loader register
		for(BufferedModel model: models) {
			loadedModels.remove(model);
		}
	}
}
