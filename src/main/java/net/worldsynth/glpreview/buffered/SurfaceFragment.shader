/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
#version 150

uniform float lightIntensity;

in vec4 Color;
in vec4 Normal;
in vec4 Light;

out vec4 outColor;

void main()
{
    outColor = Color * max(dot(normalize(Normal), normalize(Light)), 0.2) * lightIntensity;
}